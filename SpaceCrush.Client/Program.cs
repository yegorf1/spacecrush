﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace SpaceCrush.Client {
    public static class Program {
        [STAThread]
        static void Main() {
#if WINDOWS || LINUX
            using (var game = new Game1()) {
                game.Run();
            }
#else
            Console.WriteLine("Only Windows and Linux");
#endif
        }
    }
}
